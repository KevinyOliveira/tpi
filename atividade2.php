<?php
//"sucesso!"
echo "<h3> Exercicio 1</h3>\n";

$nomes = array("thiago", "dias");
$nomes = explode(" ", "thiago dias");
//explode == split.... but split deprecated...

//for
foreach($nomes as $nome){
    print "$nome <br>\n";
}


echo "<h3> Exercicio 2</h3>\n";

$lista = array(4,2,7,8,"pao");

for($x=0;$x<count($lista);$x++){
    print " {$lista[$x]} <br>\n  ";
}
//reverso...
for($x=count($lista);$x>=0;$x--){
    print " {$lista[$x]} <br>\n  ";
}
//asort($lista);


echo "<h3> Exercicio 3</h3>\n";
$n = "nome";
$q = "quantidade";
$v = "valor";
$total_pedido = 0;
$dolar = 1.72;

$lista_pedido = array(
    array(
        $n => "abacaxi",
        $q => 10,
        $v => 5.6
        ),
    array(
        $n => "banana",
        $q => 15,
        $v => 2.5
        ),
    array(
        $n => "laranjas",
        $q => 24,
        $v => 1.5
        ),
    array(
        "nome" => "uvas",
        "quantidade" => 2,
        "valor" => 7.8
        )
);
/*
$item = array("abacaxi", 10, 5.6);
$item[2] / $dolar * $item[1];
*/
foreach($lista_pedido as $item_pedido){

    $valor_dolar = $item_pedido["valor"] / $dolar;
    $valor_total_item = $item_pedido["quantidade"] * $valor_dolar;
    print " {$item_pedido[$n]} - {$item_pedido['quantidade']} -".
          " {$item_pedido[$valor]} - $valor_dolar <br> \n";
   $total_pedido += $valor_total_item;       
}

print "total do pedido = $total_pedido";

echo "<h3> Exercicio 4</h3>\n";

$texto = "ovo";
$pco = strrev($texto);
$texto = str_replace(" ","", $texto);
$pco = str_replace(" ","", $pco);

print "\n essa palavra $palavra " . (($texto==$pco) ? " eh " : " nao eh " ) . 
      " um palindromo...\n<br>";

$novo_texto = "";
for($x=strlen($texto);$x>=0;$x--){
    $novo_texto += $texto[$x];
}

?>